import {createSlice} from "@reduxjs/toolkit";

const initialState = {
    stickers: [
        {name: '윙크하는 무민', imgName: '1.jpg', isPick: false},
        {name: '하트하는 무민', imgName: '2.jpg', isPick: false},
        {name: '뽀뽀하는 무민', imgName: '3.jpg', isPick: false},
        {name: '청소하는 무민', imgName: '4.jpg', isPick: false},
        {name: '일기쓰는 무민', imgName: '5.jpg', isPick: false},
        {name: '수영하는 무민', imgName: '6.jpg', isPick: false}
    ]
}

const stickerSlice = createSlice({
    name: 'sticker',
    initialState,
    reducers: {
        pickSticker: (state) => {
            // 어차피 랜덤으로 뽑기 때문에 payload 필요 없음 => 자동주입임
            const randomIndex = Math.floor(Math.random() * state.stickers.length);
            // 난수 생성하기
            // 0 ~ sticker.length => 숫자 지정 안 하고 length로 처리해줘서 stickers가 늘어나도 자동으로 늘어나도록
            // outBoundRange
            state.stickers[randomIndex].isPick = true
            // 뽑혔으므로 상태 true 만들어주기
        }
    }
})

export const {pickSticker} = stickerSlice.actions
export default stickerSlice.reducer