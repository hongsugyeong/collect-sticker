'use client'

import {useDispatch, useSelector} from "react-redux";
import {pickSticker} from "@/lib/features/sticker/stickerSlice";

export default function Home() {
  const {stickers} = useSelector(state => state.sticker);
  const dispatch = useDispatch();

  const handleOpenSticker = () => {
      dispatch(pickSticker())
  }

  return (
      <main className="flex min-h-screen flex-col items-center p-24">
          <h1 className="text-8xl h1-font mb-10 font-extrabold">무민 스티커 뽑기</h1>
          <button onClick={handleOpenSticker} className="mb-10 text-2xl before:ease relative h-12 w-40 overflow-hidden border border-blue-300 bg-blue-300 text-white shadow-2xl transition-all before:absolute before:right-0 before:top-0 before:h-12 before:w-6 before:translate-x-12 before:rotate-6 before:bg-white before:opacity-10 before:duration-700 hover:shadow-blue-300 hover:before:-translate-x-40">
              <span relative="relative z-10" className="h1-font">고고</span>
          </button>
          <div>
              <ul className="h1-font text-4xl">
                  {stickers.map((sticker, index) => (
                      <li key={index}>{sticker.isPick ? sticker.name : <img src="/img/box.png" width='80px'/>}</li>
                  ))}
              </ul>
          </div>
      </main>
  );
}
