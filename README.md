<img src="/public/img/Nextjs.png">

# 무민 스티커 뽑기

## 프로젝트 간단 설명
Open [http://localhost:3000](http://localhost:3000)

원하는 스티커를 뽑아보세요 <br>
하지만 스티커는 랜덤이니 운을 시험해보세요!

### 프로젝트 실행 화면

#### 첫번째 화면
<img src="/public/img/sticker-collect-1.png">
버튼을 누르기 전 화면입니다 <br>
스티커를 갖고 싶다면 얼른 버튼을 눌러보세요


#### 두번째 화면
<img src="/public/img/sticker-collect-2.png">
버튼을 누르셨군요? <br>
원하는 스티커가 나왔나요? <br>
기회는 여러번이니 원하는 스티커가 나오지 않아도 걱정하지 마세요! <br>

### 사용한 기술
- Redux
- Redux Toolkit
- Redux persist
- React Developer Tools
- Random